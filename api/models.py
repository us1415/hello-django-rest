from django.db import models
from django.contrib.auth.models import User


class Client(User):
    def __str__(self):
        return self.username

    class Meta:
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'


class Auto(models.Model):
    owner = models.ForeignKey(Client, related_name='autos')
    brand = models.CharField('Марка', max_length=255, blank=True, null=True)
    model = models.CharField('Модель', max_length=255, blank=True, null=True)
    number = models.CharField('Номер', max_length=255, blank=True, null=True)

    def __str__(self):
        return self.brand

    class Meta:
        verbose_name = 'Автомобиль'
        verbose_name_plural = 'Автомобили'
