from rest_framework import generics
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework import exceptions
from random import randint

from api.serializers import ClientSerializer, AutoSerializer
from api.models import Auto


class ClientJoinView(generics.CreateAPIView):
    serializer_class = ClientSerializer
    permission_classes = [AllowAny, ]
    authentication_classes = []

    def create(self, request, *args, **kwargs):
        password = randint(11111111, 99999999)
        user_data = {
            'username': request.data.get('username'),
            'password': password,
            'first_name': request.data.get('first_name', ''),
            'last_name': request.data.get('last_name', ''),
            'email': request.data.get('email', ''),
        }
        serializer = self.get_serializer(data=user_data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(password, headers=headers)


class AutoListView(generics.ListCreateAPIView):
    serializer_class = AutoSerializer

    def get_queryset(self):
        if not hasattr(self.request.user, 'client'):
            raise exceptions.NotAcceptable
        queryset = Auto.objects.all()
        return queryset.filter(owner=self.request.user.client)

    def perform_create(self, serializer):
        if not hasattr(self.request.user, 'client'):
            raise exceptions.NotAcceptable
        serializer.save(owner=self.request.user.client)


class AutoDetailView(generics.RetrieveAPIView):
    serializer_class = AutoSerializer

    def get_queryset(self):
        if not hasattr(self.request.user, 'client'):
            raise exceptions.NotAcceptable
        queryset = Auto.objects.all()
        return queryset.filter(owner=self.request.user.client)
