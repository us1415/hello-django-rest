from rest_framework.serializers import ModelSerializer, PrimaryKeyRelatedField

from api.models import Client, Auto


class ClientSerializer(ModelSerializer):
    class Meta:
        model = Client

    def create(self, validated_data):
        client = Client(**validated_data)
        client.set_password(validated_data['password'])
        client.save()
        return client

    def update(self, instance, validated_data):
        del validated_data['username']
        instance = super().update(instance, validated_data)
        password = validated_data.get('password', False)
        if password:
            instance.set_password(password)
            instance.save()
        return instance


class AutoSerializer(ModelSerializer):
    owner = PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = Auto
