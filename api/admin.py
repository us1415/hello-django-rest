from django.contrib import admin

from api.models import Client, Auto


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    pass


@admin.register(Auto)
class AutoAdmin(admin.ModelAdmin):
    pass
