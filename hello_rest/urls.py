from django.conf.urls import url
from django.contrib import admin
from rest_framework.authtoken.views import obtain_auth_token

from api import views


urlpatterns = [
    url(r'^join/', views.ClientJoinView.as_view(), name='join'),
    url(r'^auto/$', views.AutoListView.as_view(), name='auto'),
    url(r'^auto/(?P<pk>[0-9]+)/$', views.AutoDetailView.as_view(), name='auto_detail'),
    url(r'^auth/', obtain_auth_token, name='auth'),
    url(r'^admin/', admin.site.urls),
]
